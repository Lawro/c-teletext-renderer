#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "neillsdl2.h"

/* Cells in Mode 7 display */
#define TELEHT 25
#define TELEWDTH 40

/* RGB max/min values */
#define RGB_MAX 255
#define RGB_MIN 0

/* Offsets for fonts and sixels */
#define SXL_OXLEFT 0
#define SXL_OYTOP 0
#define SXL_OXRGHT CELLW/2
#define SXL_OYMID CELLH/3
#define SXL_OYBTM (CELLH/3)*2

/* For text/graphics conversion */
#define EIGHTH_BIT 128

/* Control codes and related */
enum ctrl_codes {
   alpha_red = 129,
   alpha_green = 130,
   alpha_yellow = 131,
   alpha_blue = 132,
   alpha_magenta = 133,
   alpha_cyan = 134,
   alpha_white = 135,
   begin_alpha = alpha_red,
   end_alpha = alpha_white,
   nml_height = 140,
   dbl_height = 141,
   gfx_red = 145,
   gfx_green = 146,
   gfx_yellow = 147,
   gfx_blue = 148,
   gfx_magenta = 149,
   gfx_cyan = 150,
   gfx_white = 151,
   begin_gfx = gfx_red,
   end_gfx = gfx_white,
   contig_gfx = 153,
   sprtd_gfx = 154,
   bg_black = 156,
   bg_new = 157,
   held_gfx = 158,
   release_gfx = 159,
   begin_sxl = 160,
   begin_caps = 192,
   end_caps = 223,
   end_sxl = 255
};

/* Stores per-cell status at control code parsing phase,
   to be deciphered in SDL draw phase */
struct params {
   int text_colour;
   int dbl_ht;
   int is_block;
   int block_colour;
   int is_contig;
   int bg_colour;
   int is_held;
   int prev_gfx;
};
typedef struct params params;

/* Stores initial Mode 7 control/related data, cell x&y 
   coords for cell position in SDL, status data */
struct msev {
   int c;
   int x;
   int y;
   params prms;
};
typedef struct msev msev;

void default_line(params* def_params);
void control(msev ctrl[TELEHT][TELEWDTH]);
void set_params(params* crnt_prms, msev *crnt_cell);
void set_rgb(SDL_Simplewin *sw, int colour);
void draw_modesev(SDL_Simplewin *sw, 
                 msev output[TELEHT][TELEWDTH], 
                 fntrow fontdata[FNTCHARS][FNTHEIGHT]);
void My_SDL_DrawChar(SDL_Simplewin *sw, 
                    fntrow fontdata[FNTCHARS][FNTHEIGHT], 
                    msev *txtclr, unsigned char chr, int ox, 
                    int oy, int strtht, unsigned htmod, 
                    int ymod, int ystrtch, int ypos);
void sixels(SDL_Simplewin *sw, SDL_Rect *rectangle,
           msev *blockgfx);
void draw_sxl(SDL_Simplewin *sw, SDL_Rect *rectangle, 
              msev *blockgfx, int x, int y, int block);