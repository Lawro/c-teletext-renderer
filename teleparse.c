#include "teleparse.h"

/* Loads .m7 file into 2D struct array, calls control
   function for parsing, then calls SDL draw function.
   Loads font file for SDL text oupput. */
int main(int argc, char *argv[])
{
   msev msevin[TELEHT][TELEWDTH];
   FILE *fp;
   SDL_Simplewin sw;
   fntrow fontdata[FNTCHARS][FNTHEIGHT];
   int c;
   int row = 0, col = 0;

   if(argc != 3){
      printf("Incorrect number of arguments\n");
      exit(1);
   }

   if((fp = fopen(argv[1], "r")) == NULL){
      printf("Cannot open file\n");
      exit(1);
   }

   while ((c = fgetc(fp)) != EOF){

      if(col >= TELEWDTH){
         printf("Array out of bounds\n");
         exit(1);            
      }
      if(row >= TELEHT){
         printf("Array out of bounds\n");
         exit(1);            
      }

      msevin[row][col].c = c;
      msevin[row][col].x = col;
      msevin[row][col].y = row;

      col++;
      if(col == TELEWDTH){
         col = 0;
         row++;
      }
   }

   control(msevin);

   Neill_SDL_ReadFont(fontdata, argv[2]);

   draw_modesev(&sw, msevin, fontdata);

   fclose(fp);

   return 0;
}

/* Sets default, per character values to:
   White text, single height, 
   contiguous graphics, 
   black background, release graphics */
void default_line(params* def_params)
{
   def_params->text_colour = alpha_white;
   def_params->dbl_ht = 0;
   def_params->is_block = 0;
   def_params->block_colour = gfx_white;
   def_params->is_contig = 1;
   def_params->bg_colour = bg_black;
   def_params->is_held = 0;
   def_params->prev_gfx = 0;
}

/* Checks through struct array of cells, stores
   control values in struct for later use in SDL output.
   Resets to default values at start of each new line. */
void control(msev ctrl[TELEHT][TELEWDTH])
{
   int x, y;
   params current_prms;

   for(y = 0; y < TELEHT; y++){
      default_line(&current_prms);
      for(x = 0; x < TELEWDTH; x++){
         /* Add 8th bit (128) to alphanumberics */
         if(ctrl[y][x].c < EIGHTH_BIT){
            ctrl[y][x].c += EIGHTH_BIT;
         }
         /* Alphanumeric text colour */
         if(ctrl[y][x].c >= begin_alpha && ctrl[y][x].c <= end_alpha){
            current_prms.is_block = 0;
            current_prms.text_colour = ctrl[y][x].c;
            current_prms.prev_gfx = 0;
         }
         /* Normal height */
         if(ctrl[y][x].c == nml_height){
            current_prms.dbl_ht = 0;
            current_prms.prev_gfx = 0;
         }
         /* Double height */
         if(ctrl[y][x].c == dbl_height){
            current_prms.dbl_ht = 1;
            current_prms.prev_gfx = 0;
         }
         /* Block graphics */
         if(ctrl[y][x].c >= begin_gfx && ctrl[y][x].c <= end_gfx){
            current_prms.is_block = 1;
            current_prms.text_colour = ctrl[y][x].c;
            current_prms.block_colour = ctrl[y][x].c;
            current_prms.is_contig = 1;
         }
         /* Is contiguous */
         if(ctrl[y][x].c == contig_gfx){
            current_prms.is_block = 1;
            current_prms.is_contig = 1;
         }
         /* Is separated */
         if(ctrl[y][x].c == sprtd_gfx){
            current_prms.is_block = 1;
            current_prms.is_contig = 0;
         }
         /* Black background */
         if(ctrl[y][x].c == bg_black){
            current_prms.bg_colour = ctrl[y][x].c;
            current_prms.prev_gfx = 0;
         }
         /* New background */
         if(ctrl[y][x].c == bg_new){
            current_prms.bg_colour = current_prms.text_colour;
            current_prms.prev_gfx = 0;
         }
         /* Held graphics */
         if(ctrl[y][x].c == held_gfx){
            current_prms.is_held = 1;
         }
         /* Release graphics */
         if(ctrl[y][x].c == release_gfx){
            current_prms.is_held = 0;
            current_prms.prev_gfx = 0;
         }
         /* If a sixel, store current sixel value for use
            in held graphics mode */
         if(ctrl[y][x].c >= begin_sxl && ctrl[y][x].c <= end_sxl){
            current_prms.prev_gfx = ctrl[y][x].c;
         }
         set_params(&current_prms, &ctrl[y][x]);
      }
   }
}

/* Sets all parsed parameters to the current cell */
void set_params(params* crnt_prms, msev* crnt_cell)
{
   crnt_cell->prms.text_colour = crnt_prms->text_colour;
   crnt_cell->prms.dbl_ht = crnt_prms->dbl_ht;
   crnt_cell->prms.is_block = crnt_prms->is_block; 
   crnt_cell->prms.block_colour = crnt_prms->block_colour;
   crnt_cell->prms.is_contig = crnt_prms->is_contig;
   crnt_cell->prms.bg_colour = crnt_prms->bg_colour;
   crnt_cell->prms.is_held = crnt_prms->is_held;
   crnt_cell->prms.prev_gfx = crnt_prms->prev_gfx;
}

/* Checks if bit values are set. If set, a sixel is lit,
   else the sixel is empty. Also sets control codes to 
   blank/space graphics unless held. */
void sixels(SDL_Simplewin *sw, SDL_Rect *rectangle, msev *block)
{
   int block_value;


   if(block->c > begin_sxl && !block->prms.is_held){
      block_value = block->c - begin_sxl;
   };
   if(block->c <= begin_sxl && !block->prms.is_held){
      block_value = begin_sxl;
   };
   if(block->prms.is_held){
      block_value = block->prms.prev_gfx;
   };

   /* If bits are set (1, 2, 4, 8, 16, 64) per sixel */
   if((block_value) & (1<<0)){  
      draw_sxl(sw, rectangle, block, SXL_OXLEFT, SXL_OYTOP, 1);
   }
   if((block_value) & (1<<1)){  
      draw_sxl(sw, rectangle, block, SXL_OXRGHT, SXL_OYTOP, 1);
   }
   if((block_value) & (1<<2)){  
      draw_sxl(sw, rectangle, block, SXL_OXLEFT, SXL_OYMID, 1);
   }
   if((block_value) & (1<<3)){  
      draw_sxl(sw, rectangle, block, SXL_OXRGHT, SXL_OYMID, 1);
   }
   if((block_value) & (1<<4)){  
      draw_sxl(sw, rectangle, block, SXL_OXLEFT, SXL_OYBTM, 1);
   }
   if((block_value) & (1<<6)){  
      draw_sxl(sw, rectangle, block, SXL_OXRGHT, SXL_OYBTM, 1);
   }  

   /* If bits aren't set per sixel */
   if(!((block_value) & (1<<0))){
      draw_sxl(sw, rectangle, block, SXL_OXLEFT, SXL_OYTOP, 0);
   }
   if(!((block_value) & (1<<1))){
      draw_sxl(sw, rectangle, block, SXL_OXRGHT, SXL_OYTOP, 0);
   }
   if(!((block_value) & (1<<2))){
      draw_sxl(sw, rectangle, block, SXL_OXLEFT, SXL_OYMID, 0);
   }
   if(!((block_value) & (1<<3))){
      draw_sxl(sw, rectangle, block, SXL_OXRGHT, SXL_OYMID, 0);
   }
   if(!((block_value) & (1<<4))){
      draw_sxl(sw, rectangle, block, SXL_OXLEFT, SXL_OYBTM, 0);
   }
   if(!((block_value) & (1<<6))){
      draw_sxl(sw, rectangle, block, SXL_OXRGHT, SXL_OYBTM, 0);
   }  
}

/* Prints sixels, using positional offsets. Outputs block,
   contiquous, separated sixels. */
void draw_sxl(SDL_Simplewin *sw, SDL_Rect *rectangle, 
              msev *block, int x, int y, int blck)
{
   if(block->prms.is_contig){
      rectangle->w = SXL_OXRGHT;
      rectangle->h = SXL_OYMID;
      rectangle->x = CELLW * block->x + x;
      rectangle->y = CELLH * block->y + y;
   }
   if(!block->prms.is_contig){
      rectangle->w = SXL_OXRGHT/2;
      rectangle->h = SXL_OYMID/2;
      rectangle->x = CELLW * block->x + x + CELLW/2;
      rectangle->y = CELLH * block->y + y + CELLH/6;
   }
   if(blck){
            set_rgb(sw, block->prms.block_colour);
   }
   if(!blck){
            set_rgb(sw, block->prms.bg_colour);
   }
   SDL_RenderFillRect(sw->renderer, rectangle);
}

/* Sets cell text/bg/block colours using RGB values. */
void set_rgb(SDL_Simplewin *sw, int colour)
{
   switch(colour) {
      case alpha_red :
      case gfx_red :
         Neill_SDL_SetDrawColour(sw, RGB_MAX, RGB_MIN, RGB_MIN);
      break;
      case alpha_green :
      case gfx_green :
         Neill_SDL_SetDrawColour(sw, RGB_MIN, RGB_MAX, RGB_MIN);
      break;
      case alpha_yellow :
      case gfx_yellow :
         Neill_SDL_SetDrawColour(sw, RGB_MAX, RGB_MAX, RGB_MIN);
      break;
      case alpha_blue :
      case gfx_blue :
         Neill_SDL_SetDrawColour(sw, RGB_MIN, RGB_MIN, RGB_MAX);
      break;
      case alpha_magenta :
      case gfx_magenta :
         Neill_SDL_SetDrawColour(sw, RGB_MAX, RGB_MIN, RGB_MAX);
      break;
      case alpha_cyan :
      case gfx_cyan :
         Neill_SDL_SetDrawColour(sw, RGB_MIN, RGB_MAX, RGB_MAX);
      break;
      case alpha_white :
      case gfx_white :
         Neill_SDL_SetDrawColour(sw, RGB_MAX, RGB_MAX, RGB_MAX);
      break;
      case bg_black :
         Neill_SDL_SetDrawColour(sw, RGB_MIN, RGB_MIN, RGB_MIN);
      break;
   }
}

/* Driver function to run through 2D cell array, check ranges
   of control/colour/text codes and draw as necessary in SDL. */
void draw_modesev(SDL_Simplewin *sw, msev cell[TELEHT][TELEWDTH], 
         fntrow fontdata[FNTCHARS][FNTHEIGHT])
{
   int x, y;
   SDL_Rect rectangle;
   rectangle.w = CELLW;
   rectangle.h = CELLH;

   Neill_SDL_Init(sw);

   do{

      /* SDL_Delay(MILLISECONDDELAY); */

      for(y = 0; y < TELEHT; y++){
         for(x = 0; x < TELEWDTH; x++){
            /* Draws block graphics, else re-converts text
               (sub 8th bit) to non-block graphics */
            if (cell[y][x].c >= begin_sxl && cell[y][x].c <= end_sxl){
               if(cell[y][x].prms.is_block){
                  sixels(sw, &rectangle, &cell[y][x]);
               }
               else{
                  cell[y][x].c -= EIGHTH_BIT;
               }
            }
            /* Draws capital letters for blast-through graphics */
            if (cell[y][x].c >= 192 && cell[y][x].c <= 223){
                  cell[y][x].c -= EIGHTH_BIT;
                  My_SDL_DrawChar(sw, fontdata, &cell[y][x],
                                 cell[y][x].c, x*CELLW, y*CELLH, 
                                 0, 1, 1, 1, 0);
            }
            /* Draws alphanumeric text in single, top half
               of double, and bottom half of double height
               passing in size/position modifiers */
            if (cell[y][x].c < EIGHTH_BIT){
               if (cell[y][x].prms.dbl_ht && !cell[y-1][x].prms.dbl_ht
                                          && y-1 >= 0){
                  My_SDL_DrawChar(sw, fontdata, &cell[y][x], 
                                  cell[y][x].c, x*CELLW, y*CELLH, 
                                  0, 1, 2, 2, 0);
               }
               if (cell[y][x].prms.dbl_ht && cell[y-1][x].prms.dbl_ht
                                          && y-1 >= 0){
                  sixels(sw, &rectangle, &cell[y][x]);           
                  My_SDL_DrawChar(sw, fontdata, &cell[y][x], 
                                 cell[y][x].c, x*CELLW, y*CELLH, 
                                 FNTHEIGHT, 2, 1, 2, FNTHEIGHT);
               }
               if (!cell[y][x].prms.dbl_ht){
                  My_SDL_DrawChar(sw, fontdata, &cell[y][x],
                                 cell[y][x].c, x*CELLW, y*CELLH, 
                                 0, 1, 1, 1, 0);
               }
            }

            /* Draws control codes in release mode */
            if(cell[y][x].c >= alpha_red && cell[y][x].c < begin_sxl){
                  sixels(sw, &rectangle, &cell[y][x]);
            }

            /* Draws held graphics */
            if(cell[y][x].prms.is_held){
               sixels(sw, &rectangle, &cell[y][x]);
            }
         }
      }

      Neill_SDL_UpdateScreen(sw);

      Neill_SDL_Events(sw);

   }while(!sw->finished);

   atexit(SDL_Quit);

}

/* Modified version of Neill_SDL_DrawChar to handle per-cell
   colour data and different offsets for single/double height */
void My_SDL_DrawChar(SDL_Simplewin *sw, 
                    fntrow fontdata[FNTCHARS][FNTHEIGHT], 
                    msev *txtclr, unsigned char chr, int ox, 
                    int oy, int strtht, unsigned htmod, 
                    int ymod, int ystrtch, int ypos)
{
   unsigned x, y;
   for(y = strtht; y < FNTHEIGHT*htmod; y++){
      for(x = 0; x < FNTWIDTH*ymod; x++){
         if(fontdata[chr-FNT1STCHAR][y/ystrtch] >> (FNTWIDTH - 1 - x) & 1){
            set_rgb(sw, txtclr->prms.text_colour);
            SDL_RenderDrawPoint(sw->renderer, x + ox, y+oy - ypos);
         }
         else{
            set_rgb(sw, txtclr->prms.bg_colour);
            SDL_RenderDrawPoint(sw->renderer, x + ox, y+oy);
         }
      }
   }
}