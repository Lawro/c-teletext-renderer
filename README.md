TeleText MODE 7 Renderer

----------------------------------------------------------------------------------------------------

Description:

A C program to parse a hex-encoded MODE 7 TeleText file and render it to the screen using the
SDL2 library.

----------------------------------------------------------------------------------------------------

Pre-requisites:

SDL2

----------------------------------------------------------------------------------------------------

Compile options:

Use Makefile

----------------------------------------------------------------------------------------------------

Execution:

Run teleparse.c with arguments: the m7 file to be parsed and the MODE 7 font file